function xetDiem(){
    var diemChuan = document.getElementById("diemChuan").value;
    var diem1 = document.getElementById("diem1").value;
    var diem2 = document.getElementById("diem2").value;
    var diem3 = document.getElementById("diem3").value;
    var khuVuc = document.getElementById("khuVuc").value;
    var doiTuong = document.getElementById("doiTuong").value;
    var ketQuaXetTuyen = "";
    var diemXet = parseFloat(diem1) + parseFloat(diem2) + parseFloat(diem3) + parseFloat(khuVuc) + parseFloat(doiTuong);
    if(diem1 == "" || diem1 == 0 || diem2 == "" || diem2 == 0 || diem3 == "" || diem3 == 0){
        ketQuaXetTuyen = "Rớt";
    }else{
        ketQuaXetTuyen = diemXet>=diemChuan ? "Đậu" : "Rớt";
    }
    document.getElementById("ketQuaTuyenSinh").value = ketQuaXetTuyen;
}

function tinhTienDien(){
    var soDien = document.getElementById("soDien").value;
    let tienDien = 0;

    if (soDien <= 50) {
    tienDien = soDien * 500;
    } else if (soDien <= 100) {
    tienDien = 50 * 500 + (soDien - 50) * 650;
    } else if (soDien <= 200) {
    tienDien = 50 * 500 + 50 * 650 + (soDien - 100) * 850;
    } else if(soDien <= 350){
    tienDien = 50 * 500 + 50 * 650 + 100 * 850 + (soDien - 200) * 1100;
    } else {
    tienDien = 50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (soDien - 350) * 1300;
    }
    var tienDienFormat = new Intl.NumberFormat('vn-VN').format(tienDien);
    document.getElementById("tienDien").value = tienDienFormat + ' vnd';
}

function tinhThue(){
    var hoTen = document.getElementById("hoTen").value;
    var tongThuNhapNam = document.getElementById("tongThuNhapNam").value;
    var soNguoiPhuThuoc = document.getElementById("soNguoiPhuThuoc").value;
    var tien = parseFloat(tongThuNhapNam) - 4000000 - (parseFloat(soNguoiPhuThuoc) * 1600000)
    if(tien <=60000000){
        thue = 0.05;
    }else if(tien >60000000&&tien<=120000000){
        thue = 0.1;
    }else if(tien >120000000&&tien<=210000000){
        thue = 0.15;
    }else if(tien >210000000&&tien<=384000000){
        thue = 0.2;
    }else if(tien >384000000&&tien<=624000000){
        thue = 0.25;
    }else if(tien >624000000&&tien<=960000000){
        thue = 0.3;
    }else if(tien >960000000){
        thue = 0.35;
    }
    var tienThue = tien * thue;
    
    console.log(tien);
    console.log(thue);
    console.log(tienThue);
    tienFormat = new Intl.NumberFormat('vn-VN').format(tienThue);
    document.getElementById("thue").value ='Họ tên: ' + hoTen +'; '+ tienFormat + ' vnd';
}

function loaiKH(){
    var loaiKH = document.getElementById("KH").value;
    if(loaiKH == 0){
        document.getElementById("soKetNoiGroup").style = "display: none";
    }else{
        document.getElementById("soKetNoiGroup").style = "display: flex";
    }
}

function tinhTienCap(){
    var tienCap = 0;
    var maKH = document.getElementById("maKh").value;
    var soKenh = document.getElementById("soKenh").value;
    var soKetNoi = document.getElementById("soKetNoi").value;
    var loaiKH = document.getElementById("KH").value;
    if(soKetNoi == "" || soKetNoi == null || soKenh == "" || soKenh == null){
        soKetNoi = 0;
        soKenh = 0;
    }
    if(loaiKH == 0){
        tienCap = 4.5 + 20.5 + (parseFloat(soKenh) * 7.5);
    }else{
        if(soKetNoi >=10){
            tienCap = 15 + (parseFloat(soKenh) * 50) + 75 + (parseFloat(soKetNoi-10) * 5);
        }else{
            tienCap = 15 + (parseFloat(soKenh) * 50) + 75;
        }
    }
    var tienCapFormat = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD' }).format(tienCap)
    document.getElementById("tienCap").value ='Mã khách hàng: ' + maKH + '; ' + tienCapFormat;
}